<?php

declare(strict_types=1);

namespace App;

class SkeletonClass
{
    /**
     * Create a new Skeleton Instance
     */
    public function __construct()
    {
        // constructor body
    }

    /**
     * Friendly welcome
     *
     * @param string $phrase Phrase to return
     *
     * @return string Returns the phrase passed in
     */
    public function echoPhrase(string $phrase): string
    {
        return $phrase;
    }

    /**
     * Echo the passed in bool
     *
     * @param bool $bool Bool to return
     *
     * @return bool Returns the bool passed in
     */
    public function echoBool(bool $bool): bool
    {
        return $bool;
    }

    public function doSomething($continue = true)
    {
        if ($continue)
            $this->doSomething(false);

        return $continue;
    }
}
