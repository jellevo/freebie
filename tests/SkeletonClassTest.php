<?php

namespace App\Tests;

use App\SkeletonClass;
use PHPUnit\Framework\TestCase;

class SkeletonClassTest extends TestCase
{
    /**
     * @var SkeletonClass
     */
    private $skeletonClass;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->skeletonClass = new SkeletonClass();
    }

    /**
     * Simple test echo phrase
     */
    public function testEchoPhrase()
    {
        $this->assertEquals('test', $this->skeletonClass->echoPhrase('test'));
    }

    /**
     * Simple test echo bool
     */
    public function testEchoBool()
    {
        $this->assertEquals(true, $this->skeletonClass->echoBool(true));
    }
}
